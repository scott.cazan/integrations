# @gitbook/api

## 0.7.0

### Minor Changes

-   0e469e0: Bump API client to use the latest spec

## 0.6.0

### Minor Changes

-   d7d3f37: - Bump @gitbook/api client to use the latest Gitbook API spec

## 0.5.0

### Minor Changes

-   a2d3bc3: - Bump @gitbook/api client to use the latest Gitbook API spec

## 0.4.0

### Minor Changes

-   51eca27: Bump client to use the latest gitbook API spec

## 0.3.0

### Minor Changes

-   5c68046: Output both esm and cjs for @gitbook/api

## 0.2.2

### Patch Changes

-   bd01cd4: Output @gitbook/api build in ESM format

## 0.2.1

### Patch Changes

-   357eedd: Point main and types properly in package.json

## 0.2.0

### Minor Changes

-   d27de5f: Publish @gitbook/api to NPM

## null

### Patch Changes

-   f0c07cb: Throw a GitBookAPIError when api is returning a non-2XX response
-   782d91b: Add new "createInstallationClient" to initialize an API client for an installation
